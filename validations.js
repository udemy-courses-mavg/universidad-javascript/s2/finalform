function validateForm(form) {
  // User validation
  var user = form.user;

  if (user.value == "" || user.value == "Insert a user") {
    alert("You have to insert a user");
    user.focus();
    user.select();
    return false;
  }
  
  // Password validation
  var password = form.password;

  if (password.value == "" || password.value.length < 3) {
    alert("You have to insert a password of at least three characters");
    password.focus();
    password.select();
    return false;
  }

  // Technology validation
  var technology = form.technology;
  var checkSelected = false;

  for (i=0; i<technology.length; i++)
    if (technology[i].checked)
      checkSelected = true;

  if (!checkSelected) {
    alert("You have to select a technology");
    return false;
  }

  // Genre validation
  var genre = form.genre;
  var radioSelected = false;

  for (i=0; i<genre.length; i++) 
    if (genre[i].checked)
      radioSelected = true;

  if (!radioSelected) {
    alert("You have to select a genre");
    return false;
  }
  
  // Occupation validation
  var occupation = form.occupation;

  if (occupation.value == "") {
    alert("You have to select a occupation");
    return false;
  }

  // Validated form
  alert("Validated form, sending data...")
  return true;
}